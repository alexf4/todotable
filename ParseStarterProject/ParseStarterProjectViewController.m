// Copyright 2011 Ping Labs, Inc. All rights reserved.

#import "ParseStarterProjectViewController.h"
#import "SomethingController.h"

@implementation ParseStarterProjectViewController
@synthesize user, password, appSelector;

- (void)dealloc {
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)signup:(id)sender {

    NSString *account = [[NSString alloc] initWithString:user.text];
    NSString *pwd = [[NSString alloc] initWithString:password.text];


    PFUser *user = [PFUser user];
    user.username = account;
    user.password = pwd;
    // user.email = @"email@example.com";

    // other fields can be set just like with PFObject
    //[user setObject:@"415-392-0202" forKey:@"phone"];


    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"add new app", @"appName",
                                                                              @"asdasdfads", @"appId",
                                                                              @"cadsliasdfaasdfafaentId", @"clientId",
                                                                              nil];



    //NSArray *apps = [NSArray arrayWithObjects:dictionary];

    NSArray *apps2  = [NSArray arrayWithObject:dictionary];


    //[user setObject:@"data" forKey:@"apps"];

    // [user setObject:apps2 forKey:@"apps"];


    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
            NSLog(@"signup succesfull");
            [user setObject:apps2 forKey:@"apps"];
            [user save] ;
        } else {
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            // Show the errorString somewhere and let the user try again.
            NSLog(errorString);
        }
    }];

}


- (IBAction)signin:(id)sender {
//    
//    AppSelection *appsViewController = [[AppSelection alloc] 
//                                        initWithNibName:@"AppSelection" 
//                                        bundle:[NSBundle mainBundle]];
//    
//    // UIViewController *apps = [AppSelection initialize];
//    
//    [self.navigationController pushViewController:appsViewController animated:TRUE];
//
//    
//
    NSString *account = [[NSString alloc] initWithString:user.text];
    NSString *pwd = [[NSString alloc] initWithString:password.text];


    // [PFUser logInWithUsernameInBackground:account password:pwd target:self selector:@selector(handleUserLogin:error:)];
    [PFUser logInWithUsernameInBackground:account password:pwd
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            // do stuff after successful login.
                                            //go to app selection

//                                            AppSelection *appsViewController = [[AppSelection alloc] 
//                                                                                       initWithNibName:@"AppSelection" 
//                                                                                       bundle:[NSBundle mainBundle]];

                                            UIViewController *controller = [[MyTableController alloc] init];

//                                            UIViewController *controller =   [[SomethingController alloc] initWithNibName:@"TestNib" bundle:[NSBundle mainBundle]];
                                            // UIViewController *apps = [AppSelection initialize];

                                            [self.navigationController pushViewController:controller animated:YES];


                                        } else {
                                            // the username or password is invalid.
                                            NSLog(@"invalid username/passowrd");
                                        }
                                    }];
}

- (IBAction)doneEditing:(id)sender {
    [sender resignFirstResponder];
}


- (IBAction)bgTouched:(id)sender {

    [user resignFirstResponder];
    [password resignFirstResponder];
}

//- (void)handleUserLogin:(PFUser *)user error:(NSError **)error {
//    if(user) {
//        NSLog(@"test");
//    } else {
//        // the username or password is invalid. 
//        NSLog(@"test");
//    }
//}


@end
