//
//  SomethingController.m
//  ParseStarterProject
//
//  Created by AKharbush on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SomethingController.h"



@implementation SomethingController

@synthesize tableView, tableController, test;

- (void)viewDidLoad {
    self.tableController = [[[MyTableController alloc] init] autorelease];
    
//    MyTableController *controller = [[[MyTableController alloc] init] autorelease];
//
//    self.tableView = controller.tableView;
//    
//    self.tableView.delegate = controller;
//    
//    self.tableView.dataSource = controller;
    
    self.tableView = self.tableController.tableView;
    self.tableView.delegate = self.tableController;
    self.tableView.dataSource = self.tableController;
    
}

@end
