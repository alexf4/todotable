//
//  SomethingController.h
//  ParseStarterProject
//
//  Created by AKharbush on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyTableController.h"
#import "SomethingController.h"

@interface SomethingController : UIViewController 
{
    IBOutlet UITableView *tableView;
     MyTableController *tableController;
    
    IBOutlet UITableViewController *test;

    
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) MyTableController *tableController;
@property (nonatomic, retain) IBOutlet UITableViewController *test;

@end
