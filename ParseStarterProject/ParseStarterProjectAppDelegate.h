// Copyright 2011 Ping Labs, Inc. All rights reserved.

#import <UIKit/UIKit.h>

@class ParseStarterProjectViewController;

@interface ParseStarterProjectAppDelegate : NSObject <UIApplicationDelegate> {

    IBOutlet UIWindow *window;
    IBOutlet UINavigationController *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet ParseStarterProjectViewController *viewController;

@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

- (void)subscribeFinished:(NSNumber *)result error:(NSError *)error;

@end
