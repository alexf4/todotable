// Copyright 2011 Ping Labs, Inc. All rights reserved.

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"
#import "AppSelection.h"

@interface ParseStarterProjectViewController : UIViewController {
    IBOutlet  UITextField *user;
    
   IBOutlet AppSelection *appSelector;
    
    IBOutlet  UITextField *password;
}

@property (nonatomic, retain) IBOutlet  UITextField *user;


@property (nonatomic, retain) IBOutlet  UITextField *password;

@property (nonatomic, retain) IBOutlet  AppSelection *appSelector;


-(IBAction)signin:(id)sender;
-(IBAction)signup:(id)sender;
-(IBAction) doneEditing:(id) sender;
-(IBAction) bgTouched:(id) sender;

@end
