//
//  AppSelection.h
//  ParseStarterProject
//
//  Created by alex kharbush on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppSelection : UIViewController
{
    IBOutlet UITableView* recentSearchesTable;

}

@property (nonatomic, retain) UITableView* recentSearchesTable;


@end
